@extends('layouts.app')

@section('content')

    @if(empty($json))
        <div class="alert alert-danger text-center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            No Results Found!
        </div>
    @endif

    @include('layouts._partials.form')

    <div class="container">
        <section class="col-xs-12 col-sm-12 col-md-12 col-md-offset-2">
            <article class="search-result row">
                <!--./ Loop through and display results for BBC programmes -->
                @foreach ($json as $view)
                <div class="col-xs-7 col-sm-7 col-md-7 excerpet">
                    <h3><a href="{{ $view->programme->pid }}"> {{ $view->title  }} </a></h3>
                    <p>  {{ $view->programme->short_synopsis  }} </p>
                    <p>  {{ $view->programme->ownership->service->title  }} </p>
                </div>
                <span class="clearfix"></span>
                @endforeach
                        <!--./ end loop -->
            </article>

        </section>
    </div>
@endsection