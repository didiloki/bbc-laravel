<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Programme Finder</h2>
            {{ Form::open(['action' => 'ProgrammeController@showAll', 'method' => 'post']) }}
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        {{ Form::text('programme', '', ['class' => 'form-control input-lg prog', 'placeholder' => 'e.g. Citizen Khan']) }}
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" name="submit" type="submit">
                            Search
                        </button>
                    </span>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>