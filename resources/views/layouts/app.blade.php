<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>BBC Radio</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css')}}" >
    <link rel="stylesheet" href="{{ asset('css/style.css')}}" >

            <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <!-- Scripts -->
    <script src="{{asset('js/app.js')}}"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
    <script src="{{asset('js/typeahead.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>


</head>
<body>
<div class="navbar navbar-inverse">
    <div class="container">
        <img src="http://www.4over4.com/blog/wp-content/uploads/2014/05/BBC-Logo.jpg" width="120" />
    </div>
</div>


@yield('content')


</body>
</html>