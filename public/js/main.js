$(function() {

    //var start_url = "http://www.bbc.co.uk/radio/programmes/a-z/by/";
    //var end_url = "/current.json";
    //
    //$('.prog').typeahead({
    //    items:4,
    //    minLength:2,
    //    updater: function (item) {
    //        /* do whatever you want with the selected item */
    //        alert("selected "+item);
    //    },
    //    source: function (typeahead, query) {
    //        /* put your ajax call here..
    //        */
    //         return $.get('programme/'+query, function (data) {
    //             return typeahead.process(data);
    //         });
    //
    //        //return dataSource;
    //    }
    //});


    $( ".prog" ).autocomplete({
        delay: 50,
        minLength: 3,
        source: function(request, response) {
            var term = request.term.replace(/\s/g, '');
            $.getJSON('programme/'+term,{
            //$.getJSON(start_url+term+end_url, {
            }, function(data) {
                // data is an array of objects and must be transformed for autocomplete to use
                var array = data.error ? [] : $.map(data, function(m) {
                    return {
                        title: m.title,
                        description: m.programme.short_synopsis
                    };
                });
                response(array);
            });
        },
        focus: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
        },
        select: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            $('.prog').val(ui.item.title);
            // navigate to the selected item's url
            //window.open(ui.item.title);
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        var $a = $("<a></a>");
        $("<span class='a-title'></span>").text(item.title).appendTo($a);
        $("<span class='a-desc'></span>").text(item.description).appendTo($a);
        return $("<li></li>").append($a).appendTo(ul);
    };
});