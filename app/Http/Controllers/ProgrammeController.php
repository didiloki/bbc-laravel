<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use View;
use Illuminate\Support\Facades\Input;

class ProgrammeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

            return view::make('welcome');

    }

    public function showAll(Request $request)
    {
        $json = $this->search($request->programme);

        return view::make('result',['json'=> $json]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search($progName)
    {
        $start_url = "http://www.bbc.co.uk/radio/programmes/a-z/by/";
        $end_url = "/current.json";

        $error = false;

        //search programme
        $progName = str_replace(' ', '', $progName);

        //check if request is empty
        if(empty($progName)){
            $error = true;
        }
        //include my query to url
        $final_url = file_get_contents($start_url.$progName.$end_url);

        //get json
        $json = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '',$final_url));

        $programList = $json->atoz->tleo_titles;
        //check if reponses is empty
        if(empty($programList)){
            $error = true;
        }

        return $programList;
    }


}
